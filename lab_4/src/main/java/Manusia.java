import java.util.ArrayList;

public class Manusia {

    private String nama;
    private int umur;
    private int uang;
    private float kebahagiaan;
    private static ArrayList<Manusia> listManusia = new ArrayList<Manusia>(); // ArrayList yang menyimpan object-object Manusia
    private boolean isDead;

    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = (float) 50.0;
        this.isDead = false;
        listManusia.add(this);
    }

    // Instansiasi object Manusia tanpa uang yang dispesifikasikan
    public Manusia(String nama, int umur) {
        this(nama, umur, 50000);
    }

    public String getNama() { return nama; }

    public int getUmur() { return umur; }

    public void setUang(int uang) { this.uang = uang; }

    public int getUang() { return uang; } 

    public void setKebahagiaan(float kebahagiaan) { this.kebahagiaan = kebahagiaan; }

    public float getKebahagiaan() { return kebahagiaan; }

    // Method pengecekan pertambahan kebahagiaan
    public void tambahKebahagiaan(float kebahagiaan) {
        if (this.kebahagiaan + kebahagiaan > 100) {
            this.setKebahagiaan(100);
        }
        else {
            this.setKebahagiaan(this.kebahagiaan + kebahagiaan);
        }
    }

    // Method pengecekan pengurangan kebahagiaan
    public void kurangKebahagiaan(float kebahagiaan) {
        if (this.kebahagiaan - kebahagiaan < 0) {
            this.setKebahagiaan(0);
        }
        else {
            this.setKebahagiaan(this.kebahagiaan - kebahagiaan);
        }
    }

    // Method untuk memberi uang ke object Manusia lainnya, tanpa menyertakan jumlah uang
    public void beriUang(Manusia penerima) {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            if (penerima.isDead == true) {
                System.out.format("%s telah tiada \n", penerima.nama);
            }
            else{
                int nilaiAsciiNama = 0;
                for (int i = 0; i < penerima.nama.length(); i++) {
                    nilaiAsciiNama += (int) penerima.nama.charAt(i);
                }
                int jumlahUangDikirim = nilaiAsciiNama * 100;

                if (this.uang - jumlahUangDikirim > 0) {
                    penerima.setUang(penerima.uang + jumlahUangDikirim);
                    this.setUang(this.uang - jumlahUangDikirim);
                    float kebahagiaanBaru = ((float) jumlahUangDikirim) / 6000;
                    this.tambahKebahagiaan(kebahagiaanBaru);
                    penerima.tambahKebahagiaan(kebahagiaanBaru);
                    System.out.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D \n",
                                    this.nama, jumlahUangDikirim, penerima.nama); 
                }
                else {
                    System.out.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'( \n",
                                      this.nama, penerima.nama);
                }   
            }
        }   
    }   

    // Method untuk memberi uang ke object Manusia lainnya dengan menyertakan nilai uang
    public void beriUang(Manusia penerima, int jumlah) {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            if (penerima.isDead == true) {
                System.out.format("%s telah tiada \n", penerima.nama);
            }
            else {
                if (this.uang >= jumlah) {
                    penerima.setUang(penerima.uang + jumlah);
                    this.setUang(this.uang - jumlah);
                    float kebahagiaanBaru = ((float) jumlah) / 6000;
                    this.tambahKebahagiaan(kebahagiaanBaru);
                    penerima.tambahKebahagiaan(kebahagiaanBaru);
                    System.out.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D \n",
                                      this.nama, jumlah, penerima.nama);
                }
                else {
                    System.out.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'( \n",
                                      this.nama, penerima.nama);
                }
            }
        }   
    }

    // Method bekerja
    public void bekerja(int durasi, int bebanKerja) {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            if (this.umur < 18) {
                System.out.format("%s belum boleh bekerja karena masih di bawah umur D: \n",
                                  this.nama);
            }
            else {
                int bebanKerjaTotal = durasi * bebanKerja;
                if (bebanKerjaTotal <= this.kebahagiaan) {
                    this.kurangKebahagiaan((float) bebanKerjaTotal);
                    int pendapatan = bebanKerjaTotal * 10000;
                    System.out.format("%s bekerja full time, total pendapatan : %d \n",
                                      this.nama, pendapatan);
                    this.setUang(this.uang + pendapatan);
                }
                else {
                    int durasiBaru = (int) (this.kebahagiaan / bebanKerja);
                    bebanKerjaTotal = durasiBaru * bebanKerja;
                    this.kurangKebahagiaan((float) bebanKerjaTotal);
                    int pendapatan = bebanKerjaTotal * 10000;
                    System.out.format("%s tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : %d \n",
                                      this.nama, pendapatan);
                    this.setUang(this.uang + pendapatan);
                }
            }
        }
    }

    // Method rekreasi
    public void rekreasi(String namaTempat) {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            int biaya = namaTempat.length() * 10000;
            if (this.uang >= biaya) {
                this.setUang(this.uang - biaya);
                float kebahagiaanBaru = (float) namaTempat.length();
                this.tambahKebahagiaan(kebahagiaanBaru);
                System.out.format("%s berekreasi di %s, %s senang :) \n",
                                this.nama, namaTempat, this.nama);
            }
            else {
                System.out.format("%s tidak mempunyai cukup uang untuk berekreasi di %s \n", 
                                this.nama, namaTempat);
            }
        }  
    }

    // Method sakit
    public void sakit(String namaPenyakit) {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            this.kurangKebahagiaan(namaPenyakit.length());
            System.out.format("%s terkena penyakit %s :O \n", this.nama, namaPenyakit);
        }
    }

    // Method meninggal
    public void meninggal() {
        if (this.isDead == true) {
            System.out.format("%s telah tiada \n", this.nama);
        }
        else {
            System.out.format("%s meninggal dengan tenang, kebahagiaan : %f \n",
                              this.nama, this.kebahagiaan);
            if (Manusia.listManusia.indexOf(this) < Manusia.listManusia.size()-1) {
                Manusia manusiaTerakhir = Manusia.listManusia.get(Manusia.listManusia.size()-1);
                manusiaTerakhir.setUang(manusiaTerakhir.uang + this.uang);
                this.setUang(0);
                System.out.format("Semua harta %s disumbangkan untuk %s \n",
                                  this.nama, 
                                  Manusia.listManusia.get(Manusia.listManusia.size()-1).nama);
            }
            else {
                System.out.format("Semua harta %s hangus \n", this.nama);
            }
            this.isDead = true;
        }
    }

    // Method untuk meng-overload method bawaan toString()
    public String toString() {
        if (this.isDead == true) {
            return "Nama \t \t: Almarhum " + this.getNama() + "\n" +
                "Umur \t \t: " + this.getUmur() + "\n" +
                "Uang \t \t: " + this.getUang() + "\n" +
                "Kebahagiaan : " + this.getKebahagiaan() + "\n";
        }
        return "Nama \t \t: " + this.getNama() + "\n" +
                "Umur \t \t: " + this.getUmur() + "\n" +
                "Uang \t \t: " + this.getUang() + "\n" +
                "Kebahagiaan : " + this.getKebahagiaan() + "\n";
    }
}