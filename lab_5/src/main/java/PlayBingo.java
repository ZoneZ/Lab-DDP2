/* Program untuk meminta input */

import java.util.Scanner;

public class PlayBingo {
    private static BingoCard[] cards; // Array berisi objek-objek BingoCard
    private static int numOfBingos = 0;

    public static BingoCard[] getCards() {
        return PlayBingo.cards;
    }

    public static void setCards(int index, BingoCard card) {
        PlayBingo.cards[index] = card;
    }

    public static int getNumOfBingos() {
        return PlayBingo.numOfBingos;
    }

    public static void setNumOfBingos(int num) {
        PlayBingo.numOfBingos += num;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan jumlah pemain beserta nama-namanya (pisahkan dengan spasi): ");   
        String setPlayers = input.nextLine();
        String[] setPlayersArr = setPlayers.split(" ");

        int numOfPlayers = Integer.parseInt(setPlayersArr[0]);
        PlayBingo.cards = new BingoCard[numOfPlayers];

        // Konfigurasi deck setiap pemain
        for (int num = 0; num < numOfPlayers; num++) {
            String playerName = setPlayersArr[num + 1];
            System.out.println("Masukkan kartu-kartu untuk " + playerName);
            Number[][] tempNumbers = new Number[5][5];
            for (int outIndex = 0; outIndex < 5; outIndex++) {
                String cardNumbers = input.nextLine();
                String[] cardNumbersArr = cardNumbers.split(" ");
                for (int inIndex = 0; inIndex < 5; inIndex++) {
                    int cardValue = Integer.parseInt(cardNumbersArr[inIndex]);
                    tempNumbers[outIndex][inIndex] = new Number(cardValue, outIndex, inIndex);
                }
            }
            BingoCard newCards = new BingoCard(playerName, tempNumbers);
            PlayBingo.setCards(num, newCards);
        }

        // Meminta input tanpa henti hingga semua pemain memenangkan permainan
        while (true) {
            if (PlayBingo.getNumOfBingos() == PlayBingo.cards.length) {
                System.out.println("Semua pemain sudah memenangkan permainan");
                System.out.println("Permainan berakhir");
                break;
            } else {
                System.out.println("Masukkan perintah: ");
                String command = input.nextLine();
                String[] commandArr = command.split(" ");

                // Memproses perintah MARK (angka)
                if (commandArr[0].equals("MARK")) {
                    int num = Integer.parseInt(commandArr[1]);
                    for (BingoCard player: PlayBingo.cards) {
                        if (player.getBingo()) {
                            System.out.println(player.getName() + " telah memenangkan permainan");
                        } else {
                            String message = player.markNum(num);
                            System.out.println(player.getName() + ": " + message);
                            player.isBingo();
                            if (player.getBingo()) {
                                System.out.println(player.getName() + " menang!");
                                PlayBingo.setNumOfBingos(1);
                            }
                        }   
                    } 

                // Memproses perintah INFO (nama)
                } else if (commandArr[0].equals("INFO")) {
                    String name = commandArr[1];
                    for (BingoCard player: PlayBingo.cards) {
                        if (player.getName().equals(name)) {
                            if (player.getBingo()) {
                                System.out.println(player.getName() + " telah memenangkan permainan");
                            } else {
                                System.out.println(name);
                                System.out.println(player.info());
                            }
                        } 
                    }

                // Memproses perintah RESTART (nama)
                } else if (commandArr[0].equals("RESTART")) {
                    String name = commandArr[1];
                    for (BingoCard player: PlayBingo.cards) {
                        if (player.getName().equals(name)) {
                            if (player.getBingo()) {
                                System.out.println(player.getName() + " telah memenangkan permainan");
                            } else {
                                if (player.getHasRestarted()) {
                                    System.out.println(player.getName() + " sudah pernah mengajukan RESTART");
                                } else {
                                    for (BingoCard restartedPlayer: PlayBingo.cards) {
                                        restartedPlayer.restart();
                                    }
                                    player.setHasRestarted(true);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    System.out.println("Incorrect command");
                }
            }
        }
    }
}