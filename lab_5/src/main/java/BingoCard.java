/*
 * BingoCard Template created by Nathaniel Nicholas
 * Adopted by Farhan Azyumardhi Azmi
 */

public class BingoCard {

	private String name; // Attribute khusus implementasi multi-player
	private Number[][] numbers; 
	private Number[] numberStates;
	private boolean isBingo;
	private String[][] usedNumbers = new String[5][5];
	private boolean hasRestarted = false;
	
	// Constructor method untuk implementasi class PlayBingo
	public BingoCard(String name, Number[][] numbers) {
		this.name = name;
		this.numbers = numbers;
		this.isBingo = false;
		for (int i = 0; i < this.getNumbers().length; i++) {
			for (int j = 0; j < this.getNumbers().length; j++) {
				this.setusedNumbers(i, j, String.valueOf(this.getNumbers()[i][j].getValue()));
			}
		}
	}

	// Constructor method overload khusus untuk implementasi BingoCardTest.java
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
		for (int i = 0; i < this.getNumbers().length; i++) {
			for (int j = 0; j < this.getNumbers().length; j++) {
				this.setusedNumbers(i, j, String.valueOf(this.getNumbers()[i][j].getValue()));
			}
		}
	}

	public String getName() {
		return this.name;
	}

	public Number[][] getNumbers() {
		return this.numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public String[][] getusedNumbers() {
		return this.usedNumbers;
	}

	public void setusedNumbers(int x, int y, String value) {
		this.usedNumbers[x][y] = value;
	}

	public boolean getHasRestarted() {
		return this.hasRestarted;
	}

	public void setHasRestarted(boolean status) {
		this.hasRestarted = status;
	}

	public boolean getBingo() {
		return this.isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	// Method untuk mengubah angka pada kartu menjadi tanda X
	public String markNum(int num) {
		String resultMessage = "Kartu tidak memiliki angka " + String.valueOf(num);
		searchLoop:
		for (int i = 0; i < this.getNumbers().length; i++) {
			for (int j = 0; j < this.getNumbers().length; j++) {
				if (this.numbers[i][j].getValue() == num) {
					if (this.getusedNumbers()[i][j].equals("X ")) {
						resultMessage = String.valueOf(num) + " sebelumnya sudah tersilang";
					} else {
						this.setusedNumbers(i, j, "X ");
						resultMessage = String.valueOf(num) + " tersilang";
						break searchLoop;
					}
				}
			}
		}
		return resultMessage;
	}	
	
	// Method untuk menampilkan deck kartu pemain
	public String info() {
		String result = "";
		for (int i = 0; i < this.getNumbers().length; i++) {
			result += "| ";
			for (int j = 0; j <this.getNumbers().length; j++) {
				if (!(j == 4)) {
					result += String.valueOf(this.getusedNumbers()[i][j]) + " | ";
				} else {
					result += String.valueOf(this.getusedNumbers()[i][j]) + " |";
				}
			}
			if (!(i == 4)) {
				result += "\n";
			} 
		}
		return result;
	}

	// Method untuk me-restore deck kartu pemain (hanya bisa dilakukan sekali)
	public void restart() {
		if (this.hasRestarted == false) {
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					this.setusedNumbers(i, j, String.valueOf(this.getNumbers()[i][j].getValue()));
				}
			}
			System.out.println("Mulligan!");
			System.out.println(this.info());
		} 
	}

	// Method untuk mengecek lima tanda X secara horizontal
	public boolean bingoRowCheck() {
		for (int i = 0; i < 5; i++) {
			boolean isRowBingo = true;
			for (int j = 0; j < 5; j++) {
				isRowBingo = isRowBingo && (this.getusedNumbers()[i][j].equals("X "));
			}
			if (isRowBingo) {
				return isRowBingo;
			}
		}
		return false;
	}

	// Method untuk mengecek lima tanda X secara vertikal
	public boolean bingoColCheck() {
		for (int i = 0; i < 5; i++) {
			boolean isColBingo = true;
			for (int j = 0; j < 5; j++) {
				isColBingo = isColBingo && (this.getusedNumbers()[j][i].equals("X "));
			}
			if (isColBingo) {
				return isColBingo;
			}
		}
		return false;
	}

	// Method untuk mengecek lima tanda X secara diagonal (pojok kiri atas - pojok kanan bawah)
	public boolean bingoRightDiagCheck() {
        boolean isRightDiagBingo = true;
        for (int i = 0; i < 5; i++) {
            isRightDiagBingo = isRightDiagBingo && (this.getusedNumbers()[i][i].equals("X "));
        }
        if (isRightDiagBingo) {
            return isRightDiagBingo;
        }
        return false;
    }

	// Method untuk mengecek lima tanda X secara diagonal (pojok kiri bawah - pojok kanan atas)
    public boolean bingoLeftDiagCheck() {
        boolean isLeftDiagBingo = true;
        for (int i = 0; i < 5; i++) {
            isLeftDiagBingo = isLeftDiagBingo && (this.getusedNumbers()[4 - i][i].equals("X "));
        }
        if (isLeftDiagBingo) {
            return isLeftDiagBingo;
        }
        return false;
	}
	
	// Method untuk mengecek kondisi Bingo
	public boolean isBingo() {
		boolean bingoCheck = this.bingoRowCheck() || this.bingoColCheck()
						  || this.bingoRightDiagCheck() || this.bingoLeftDiagCheck();
		if (bingoCheck) {
			this.setBingo(true);
        	System.out.println("BINGO!");
        	System.out.println(this.info());
		}
		return this.isBingo;
	}
}