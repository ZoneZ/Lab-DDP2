package karyawan;

public abstract class Karyawan {

	protected String nama;
	protected double gaji;
	protected int counterGaji;

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNama() {
		return nama;
	}

	public void setGaji(double gaji) {
		this.gaji = gaji;
	}

	public double getGaji() {
		return gaji;
	}

	public void setCounterGaji(int counterGaji) {
		this.counterGaji = counterGaji;
	}

	public int getCounterGaji() {
		return counterGaji;
	}
}