package karyawan;

public class Staff extends Manager {

	private final double BATAS_GAJI = 18000;

	public double getBATAS_GAJI() {
		return this.BATAS_GAJI;
	}

	public Staff(String nama, double gaji) {
		super(nama, gaji);
	}

	public String tambahBawahan(Intern karyawan) {
		return super.tambahBawahan(karyawan);
	}
}