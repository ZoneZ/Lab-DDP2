package karyawan;

import java.util.ArrayList;

public class Manager extends Karyawan {

	protected ArrayList<Karyawan> bawahanList;
	protected final int LIMIT_BAWAHAN = 10;

	public ArrayList<Karyawan> getBawahan() {
		return bawahanList;
	}

	/** Method untuk menambah bawahan ke dalam ArrayList object yang memanggil method ini.
	 * @param bawahan : object Karyawan yang akan ditambah (menjadi bawahan)
	 * @return String pesan berhasil atau tidaknya penambahan bawahan
	 */
	public String tambahBawahan(Karyawan bawahan) {
		String out = "";
		if (this.bawahanList.size() == this.LIMIT_BAWAHAN) {
			out += String.format("%s sudah tidak bisa menerima bawahan lagi\n", this.nama);
		} else {
			if (this.bawahanList.contains(bawahan)) {
				out += String.format("Karyawan %s telah menjadi bawahan %s\n",
						bawahan.getNama(), this.nama);
			} else {
				this.bawahanList.add(bawahan);
				out += String.format("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n",
						bawahan.getNama(), this.nama);
			}
		}
		return out;
	}

	public Manager(String nama, double gaji) {
		this.nama = nama;
		this.gaji = gaji;
		this.counterGaji = 0;
		this.bawahanList = new ArrayList<>();
	}
}