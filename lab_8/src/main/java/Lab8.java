import java.util.Scanner;
import karyawan.*;


class Lab8 {

    private static final int PANJANG_COMMAND_TAMBAH_KARYAWAN = 4;
    private static final int PANJANG_COMMAND_STATUS = 2;
    private static final int PANJANG_COMMAND_TAMBAH_BAWAHAN = 3;
    private static final int PANJANG_COMMAND_GAJI = 1;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Masukkan batas gaji promosi Staff: ");
            String thresholdCommand = input.nextLine();
            try {
                if (Double.parseDouble(thresholdCommand) > 0) {
                    Korporasi perusahaan = new Korporasi(Double.parseDouble(thresholdCommand));
                    while (true) {
                        try {
                            System.out.print("Masukkan perintah: ");
                            String command = input.nextLine();
                            String[] commandArr = command.split(" ");
            
                            if (commandArr.length == Lab8.PANJANG_COMMAND_TAMBAH_KARYAWAN
                                    && commandArr[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                                String nama = commandArr[1];
                                String jabatan = commandArr[2];
                                double gaji = Double.parseDouble(commandArr[3]);
                                if (perusahaan.cariKaryawan(nama) == null) {
                                    if (jabatan.toUpperCase().equals("MANAGER")
                                        || jabatan.toUpperCase().equals("STAFF")
                                        || jabatan.toUpperCase().equals("INTERN")) {
                                        System.out.print(perusahaan.tambahKaryawan(nama, jabatan, gaji));
                                    } else {
                                        System.out.format("%s tidak ada di PT. TAMPAN\n", jabatan);
                                    }
                                } else {
                                    System.out.format("%s sudah ada\n", nama);
                                }
            
                            } else if (commandArr.length == Lab8.PANJANG_COMMAND_STATUS
                                    && commandArr[0].toUpperCase().equals("STATUS")) {
                                String nama = commandArr[1];
                                System.out.print(perusahaan.statusKaryawan(nama));
            
                            } else if (commandArr.length == Lab8.PANJANG_COMMAND_TAMBAH_BAWAHAN
                                    && commandArr[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                                String namaAtasan = commandArr[1];
                                String namaBawahan = commandArr[2];
                                if (perusahaan.cariKaryawan(namaAtasan) == null
                                    || perusahaan.cariKaryawan(namaBawahan) == null) {
                                    System.out.println("Nama tidak berhasil ditemukan");
                                } else {
                                    Karyawan atasan = perusahaan.cariKaryawan(namaAtasan);
                                    Karyawan bawahan = perusahaan.cariKaryawan(namaBawahan);
                                    if (atasan.getClass().getSimpleName().equals("Manager")) {
                                        if (bawahan.getClass().getSimpleName().equals("Manager")) {
                                            System.out.println("Anda tidak layak memiliki bawahan");
                                        } else {
                                            System.out.print(((Manager) atasan).tambahBawahan(bawahan));
                                        }
                                    }
                                    else if (atasan.getClass().getSimpleName().equals("Staff")) {
                                        if (bawahan.getClass().getSimpleName().equals("Intern")) {
                                            System.out.print(((Staff) atasan).tambahBawahan(bawahan));
                                        } else {
                                            System.out.println("Anda tidak layak memiliki bawahan");
                                        }
                                    }
                                    else {
                                        System.out.println("Anda tidak layak memiliki bawahan");
                                    }
                                }
                            }
            
                            else if (commandArr.length == Lab8.PANJANG_COMMAND_GAJI
                                    && commandArr[0].toUpperCase().equals("GAJIAN")) {
                                System.out.print(perusahaan.gajian());
                            }
            
                            else if (commandArr.length == Lab8.PANJANG_COMMAND_GAJI
                                        && commandArr[0].toUpperCase().equals("EXIT")) {
                                System.out.println("Program berhenti");
                                System.exit(0);
                            }

                            else {
                                System.out.println("Perintah tidak valid");
                            }
                        } catch (Exception e) {
                            System.out.println("Terjadi kesalahan");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Masukkan batas promosi terlebih dahulu");
            }
        }
    }
}