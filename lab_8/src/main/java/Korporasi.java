import java.util.ArrayList;
import karyawan.*;

public class Korporasi {

	private ArrayList<Karyawan> karyawanList;
	private final String NAMA = "PT. TAMPAN";
	private final int NAIK_GAJI_COUNT = 6;
	private final double KENAIKAN_GAJI = 0.1;
	private final int LIMIT_KARYAWAN = 10000;
	private double batasPromosi;

	public ArrayList<Karyawan> getKaryawan() {
		return karyawanList;
	}

	public double getBatasPromosi() {
		return batasPromosi;
	}

	public void setBatasPromosi(double batas) {
		batasPromosi = batas;
	}

	public Korporasi(double batasPromosi) {
		this.karyawanList = new ArrayList<>();
		this.batasPromosi = batasPromosi;
	}

	/** Method untuk mencari karyawan dari suatu objek Korporasi.
	 * @param nama : nama karyawan yang ingin dicari
	 * @return objek Karyawan jika ditemukan. Return null jika tidak.
	 */
	public Karyawan cariKaryawan(String nama) {
		Karyawan returnedKaryawan = null;
		for (Karyawan karyawan : this.karyawanList) {
			if (karyawan.getNama().equals(nama)) {
				returnedKaryawan = karyawan;
 			}
		}
		return returnedKaryawan;
	}

	/** Method untuk menambah karyawan jika suatu objek Korporasi masih bisa menampungnya.
	 * @param nama : nama karyawan yang ditambah
	 * @param jabatan : jabatan dari karyawan yang ditambah
	 * @param gajiAwal : gaji dari karyawan yang ditambah
	 * @return String pesan berhasil atau tidaknya penambahan karyawan
	 */
	public String tambahKaryawan(String nama, String jabatan, double gajiAwal) {
		String out = "";
		if (this.karyawanList.size() == this.LIMIT_KARYAWAN) {
			out += "PT TAMPAN sudah tidak bisa menerima karyawan lagi\n";
		} else {
			if (jabatan.toUpperCase().equals("MANAGER")) {
				this.karyawanList.add(new Manager(nama, gajiAwal));
			} else if (jabatan.toUpperCase().equals("STAFF")) {
				this.karyawanList.add(new Staff(nama, gajiAwal));
			} else if (jabatan.toUpperCase().equals("INTERN")) {
				this.karyawanList.add(new Intern(nama, gajiAwal));
			}
			out += String.format("%s mulai bekerja sebagai %s di %s\n",
					nama, jabatan, this.NAMA);
		}
		return out;
	}

 	/** Method untuk melihat status seorang karyawan.
	 * @param nama : nama karyawan yang ingin dicetak statusnya
	 * @return String pesan berhasil atau tidaknya pengembalian status karyawan
	 */
	public String statusKaryawan(String nama) {
		String out = "";
		if (this.cariKaryawan(nama) != null) {
			out += String.format("%s %.3f\n", this.cariKaryawan(nama).getNama(),
					this.cariKaryawan(nama).getGaji());
		} else {
			out += "Karyawan tidak ditemukan\n";
		}
		return out;
	}

	/** Method gajian. Proses kenaikan gaji dan promosi Staff juga dilakukan di sini.
	 * @return String pesan gajian beserta dinamikanya
	 */
	public String gajian() {
		String out = "";
		if (this.karyawanList.size() == 0) {
			out += "Tidak ada satupun karyawan\n";
		} else {
			out += "Semua karyawan telah diberikan gaji\n";
			for (Karyawan karyawan : this.karyawanList) {
				karyawan.setCounterGaji(karyawan.getCounterGaji() + 1);
				if (karyawan.getCounterGaji() == this.NAIK_GAJI_COUNT) {
					double gajiBaru = karyawan.getGaji() + (karyawan.getGaji() * this.KENAIKAN_GAJI);
					out += String.format("%s mengalami kenaikan gaji sebesar 10%% dari %.3f menjadi %.3f\n",
							karyawan.getNama(), karyawan.getGaji(), gajiBaru);
					karyawan.setGaji(gajiBaru);
					karyawan.setCounterGaji(0);
				}

				if (karyawan.getClass().getSimpleName().equals("Staff")
					&& karyawan.getGaji() > this.batasPromosi) {
						out += String.format("Selamat, %s telah dipromosikan menjadi MANAGER\n",
								karyawan.getNama());
						for (Karyawan atasan : this.karyawanList) {
							if (atasan.getClass().getSimpleName().equals("Manager")
								|| atasan.getClass().getSimpleName().equals("Staff")) {
									for (int i = 0; i < ((Manager) atasan).getBawahan().size(); i++) {
										if (((Manager) atasan).getBawahan().get(i).getNama().equals(karyawan.getNama())) {
											out += String.format("%s sudah tidak menjadi bawahan dari %s\n",
													karyawan.getNama(), atasan.getNama());
											((Manager) atasan).getBawahan().remove(i);
										}
									}
							}
						}
						this.karyawanList.set(this.karyawanList.indexOf(karyawan), 
											new Manager(karyawan.getNama(), karyawan.getGaji()));
				}
			}
		}
		return out;
	}
}