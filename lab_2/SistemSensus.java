import java.util.InputMismatchException;
import java.util.Scanner;

 /*Code Author (Mahasiswa):
Farhan Azyumardhi Azmi, NPM 1706979234, Kelas D, GitLab Account: https://gitlab.com/ZoneZ
 */

public class SistemSensus {

	//Static method untuk memeriksa nilai panjang, lebar, dan tinggi
	public static void cekPanjangLebarTinggi(short parameter){
		if (parameter <= 0 || parameter > 250){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi");
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		//Membuat input scanner baru
		Scanner input = new Scanner(System.in);

		try {
			/*Menampilkan User Interface untuk meminta masukan, jika ada variabel yang tidak
			memenuhi kriteria, maka pesan peringatan dimunculkan dan program langsung ditutup*/
			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			String nama = input.nextLine();

			System.out.print("Alamat Rumah           : ");
			String alamat = input.nextLine();

			System.out.print("Panjang Tubuh (cm)     : ");
			short panjang = input.nextShort();
			cekPanjangLebarTinggi(panjang);
		
			System.out.print("Lebar Tubuh (cm)       : ");
			short lebar = input.nextShort();
			cekPanjangLebarTinggi(lebar);

			System.out.print("Tinggi Tubuh (cm)      : ");
			short tinggi = input.nextShort();
			cekPanjangLebarTinggi(tinggi);

			System.out.print("Berat Tubuh (kg)       : ");
			float berat = input.nextFloat();
			if (berat <= 0 || berat > 150){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}

			System.out.print("Jumlah Anggota Keluarga: ");
			byte jumlahKeluarga = input.nextByte();
			if (jumlahKeluarga <= 0 || jumlahKeluarga > 20){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}

			input.nextLine();

			System.out.print("Tanggal Lahir          : ");
			String tanggalLahir = input.nextLine();

			System.out.print("Catatan Tambahan       : ");
			String catatan = input.nextLine();

			System.out.print("Jumlah Cetakan Data    : ");
			short jumlahCetakan = input.nextShort();

			input.nextLine();
			System.out.println("");

			//Menghitung rasio berat per volume (rumus lihat soal)
			float rasio = (berat / (((float) panjang / 100) * ((float) lebar / 100) * ((float) tinggi / 100)));
			int rasioInt = (int) rasio; //"Membulatkan" hasil perhitungan rasio

			for (int i = 1; i < jumlahCetakan+1; i++) {
				//Meminta masukan terkait nama penerima hasil cetak data
				System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
				String penerima = input.nextLine().toUpperCase(); //Membaca input lalu langsung dijadikan uppercase

			//Mencetak hasil
			System.out.print("DATA SIAP DICETAK UNTUK " + penerima + " \n" + 
							"-----------------\n" + 
							nama + " - " + alamat + " \n" +
							"Lahir pada tanggal " + tanggalLahir + " \n" +
							"Rasio Berat Per Volume    = " + rasioInt + " kg/m^3" + "\n");
			//Pengecekan Catatan Tambahan
			if (catatan != null && catatan.length() != 0) System.out.println("Catatan: " + catatan);
			else System.out.println("Tidak ada catatan tambahan");
			System.out.println("");
			}

			//Menggabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
			String hurufPertamaNama = String.valueOf(nama.charAt(0));
			//Mendapatkan konversi ASCII dari nama kepala keluarga
			int asciiNama = 0;
			for (int i = 0; i < nama.length(); i++){
				asciiNama += (int) nama.charAt(i);
			}

			float kalkulasi = ((panjang * tinggi * lebar) + asciiNama) % 10000;
			int intKalkulasi = (int) kalkulasi;
			String nomorKeluarga = hurufPertamaNama + String.valueOf(intKalkulasi);

			//Menghitung anggaran makanan per tahun (rumus lihat soal)
			int anggaran = 50000 * 365 * jumlahKeluarga;

			//Menghitung umur dari tanggalLahir (rumus lihat soal)
			String[] tanggalLahirSplit = tanggalLahir.split("-"); //Meng-"split" tanggal lahir menjadi array of Strings
			short tahunLahir = Short.parseShort(tanggalLahirSplit[2]); 
			short umur = (short) (2018 - tahunLahir);

			//Melakukan proses menentukan apartemen (kriteria lihat soal)
			String namaApartemen = "";
			String kabupaten = "";
			if (umur >= 0 && umur <= 18){
				namaApartemen = "PPMT";
				kabupaten = "Rotunda";
			}
			else {
				if (anggaran > 100000000){
					namaApartemen = "Mares";
					kabupaten = "Margonda";
				}
				else {
					namaApartemen = "Teksas";
					kabupaten = "Sastra";
				}
			}
			//Mencetak rekomendasi apartemen
			System.out.print("REKOMENDASI APARTEMEN\n" + 
							"---------------------\n" +
							"MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" + 
							"MENIMBANG: Anggaran makanan tahunan: Rp " + anggaran + "\n" +
							"           Umur kepala keluarga: " + umur + " tahun\n" +
							"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di: " + "\n" +
							namaApartemen + ", kabupaten " + kabupaten + "\n");
			input.close();
		}
		
		catch (InputMismatchException e){
			System.out.println("WARNING: Input salah. Keluarga tidak perlu direlokasi!");
			System.exit(0);
		}
	}
}