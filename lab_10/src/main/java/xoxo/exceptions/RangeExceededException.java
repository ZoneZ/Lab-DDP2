package xoxo.exceptions;

/**
 * An exception that is thrown if the Seed value
 * is not between 0 - 36 (inclusive) in range.
 */
public class RangeExceededException extends RuntimeException {

	/**
	 * Class constructor
	 *
	 * @param message : message to be printed by the exception.
	 */
	public RangeExceededException(String message) {
		super(message);
	}
}