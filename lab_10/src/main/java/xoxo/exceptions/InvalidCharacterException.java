package xoxo.exceptions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An exception that is thrown if the Kiss Key contains
 * at least one character except character 'A' to 'Z', 'a' to 'z', and '@'.
 */
public class InvalidCharacterException extends RuntimeException {

	/**
	 * Class constructor.
	 *
	 * @param message : message to be printed by the exception.
	 */
	public InvalidCharacterException(String message) {
		super(message);
	}

	/**
	 * Method to check if the key contains at least one illegal character
	 *
	 * @param key : the key to be checked
	 * @return : true if the key contains at least one illegal character,
	 * else if otherwise
	 */
	public static boolean containsIllegalChar(String key) {
		Pattern pattern = Pattern.compile("[^a-zA-Z@]");
		Matcher matcher = pattern.matcher(key);
		return matcher.find();
	}
}