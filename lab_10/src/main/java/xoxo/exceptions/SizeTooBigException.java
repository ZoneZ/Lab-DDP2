package xoxo.exceptions;

/**
 * An exception that is thrown if the size of the message is greater than 10 KiloBit.
 */
public class SizeTooBigException extends RuntimeException {

	private static final int SIZE_LIMIT = 1250;

	public static int getSizeLimit() {
		return SIZE_LIMIT;
	}

	/**
	 * Class constructor.
	 *
	 * @param message : message to be printed by the exception.
	 */
	public SizeTooBigException(String message) {
		super(message);
	}
}