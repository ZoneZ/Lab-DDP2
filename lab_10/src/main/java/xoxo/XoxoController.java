package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Farhan Azyumardhi Azmi - 1706979234
 */
public class XoxoController {

	/**
	 * The GUI object that can be used to get
	 * and show the data from and to users.
	 */
	private XoxoView gui;

	/**
	 * Class constructor given the GUI object.
	 */
	public XoxoController(XoxoView gui) {
		this.gui = gui;
	}

	/**
	 * Main method that runs all the business process.
	 */
	public void run() {
		this.gui.setEncryptFunction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String messageText = gui.getMessageText();
				String keyText = gui.getKeyText();
				String seed = gui.getSeedText();
				encryptMessage(messageText, keyText, seed);
			}
		});

		this.gui.setDecryptFunction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String messageText = gui.getMessageText();
				String keyText = gui.getKeyText();
				String seed = gui.getSeedText();
				decryptMessage(messageText, keyText, seed);
			}
		});

		this.gui.setBrowseButtonFunction(e -> browseFiles());
	}

	/**
	 * A method that runs when "ENCRYPT!" button is pressed, the resulting
	 * encrypted message its Hug Key, and seed will be written on a XoXoEncryptedMessage.enc file.
	 *
	 * @param messageText : message to be encrypted, obtained from GUI input.
	 * @param keyText     : key to encrypt to message, obtained from GUI input.
	 * @param seedText    : seed that will be paired with the hug key.
	 */
	public void encryptMessage(String messageText, String keyText, String seedText) {
		if (messageText.equals("") && keyText.equals("")) {
			JOptionPane.showMessageDialog(null, "No message and key found.");
		} else if (messageText.equals("")) {
			JOptionPane.showMessageDialog(null, "No message found.");
		} else if (keyText.equals("")) {
			JOptionPane.showMessageDialog(null, "No key found.");
		} else {
			try {
				XoxoEncryption encryptionObject = new XoxoEncryption(keyText);
				XoxoMessage xoxoMessageObject;
				if (seedText.equals("") || seedText.equalsIgnoreCase("DEFAULT_SEED")) {
					xoxoMessageObject = encryptionObject.encrypt(messageText);
					seedText = String.valueOf(HugKey.DEFAULT_SEED);
				} else {
					xoxoMessageObject = encryptionObject.encrypt(messageText, Integer.parseInt(seedText));
				}
				String encryptedMessage = xoxoMessageObject.getEncryptedMessage();
				String hugKeyString = xoxoMessageObject.getHugKey().getKeyString();
				this.writeToFile("XoXoEncryptedMessages.enc",
						encryptedMessage, hugKeyString, seedText);
			} catch (RangeExceededException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (InvalidCharacterException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (KeyTooLongException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (SizeTooBigException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Invalid seed input!");
				this.gui.appendLog("Invalid seed input!");
			}
		}
	}

	/**
	 * A method that runs when "DECRYPT!" button is pressed or when .enc file(s) is selected,
	 * the resulting decrypted message will be written on a XoXoDecryptedMessage.txt.
	 *
	 * @param messageText : message to be decrypted.
	 * @param keyText     : hug key that will be used to decrypt the message.
	 * @param seedText    : seed that will be used for decryption.
	 */
	public void decryptMessage(String messageText, String keyText, String seedText) {
		if (messageText.equals("") && keyText.equals("")) {
			JOptionPane.showMessageDialog(null, "No message and key found.");
		} else if (messageText.equals("")) {
			JOptionPane.showMessageDialog(null, "No message found.");
		} else if (keyText.equals("")) {
			JOptionPane.showMessageDialog(null, "No key found.");
		} else {
			try {
				if (messageText.getBytes(StandardCharsets.UTF_8).length
						> SizeTooBigException.getSizeLimit()) {
					throw new SizeTooBigException("Message is too big to handle.");
				}
				XoxoDecryption decryptionObject = new XoxoDecryption(keyText);
				String decryptedMessage;
				if (seedText.equals("") || seedText.equalsIgnoreCase("DEFAULT_SEED")) {
					decryptedMessage = decryptionObject.decrypt(messageText, HugKey.DEFAULT_SEED);
				} else {
					if (Integer.parseInt(seedText) < HugKey.MIN_RANGE
							|| Integer.parseInt(seedText) > HugKey.MAX_RANGE) {
						throw new RangeExceededException("Seed value is not in range 0 - 36 (inclusive).");
					} else {
						decryptedMessage = decryptionObject.decrypt(messageText, Integer.parseInt(seedText));
					}
				}
				this.writeToFile("XoXoDecryptedMessages.txt", decryptedMessage);
			} catch (RangeExceededException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (SizeTooBigException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				this.gui.appendLog(e.getMessage());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Invalid seed input!");
				this.gui.appendLog("Invalid seed input!");
			}
		}
	}

	/**
	 * Writes encrypted message, its Hug Key, and seed to XoXoEncryptedMessages.enc
	 *
	 * @param fileName : filename.
	 * @param message  : encrypted message.
	 * @param key      : Hug Key.
	 * @param seed     : seed.
	 */
	public void writeToFile(String fileName, String message, String key, String seed) {
		File file = new File(fileName);
		FileWriter writer = null;
		try {
			writer = new FileWriter(file, true);
			writer.write(message + " " + key + " " + seed);
			writer.write(System.lineSeparator());
			this.gui.appendLog("Process completed! See output file for results.");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error writing output to a file.");
			this.gui.appendLog("Error writing output to a file.");
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error processing file.");
				this.gui.appendLog("Error processing file.");
			}
		}
	}

	/**
	 * Writes decrypted message to XoXoDecryptedMessages.txt
	 *
	 * @param fileName : filename.
	 * @param message  : decrypted message.
	 */
	public void writeToFile(String fileName, String message) {
		this.writeToFile(fileName, message, "", "");
	}

	/**
	 * Method that runs when the "Browse .enc Files..." button is pressed.
	 * It constructs a JFileChooser object for file selections.
	 * Multiple files selection is supported.
	 */
	public void browseFiles() {
		JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getDefaultDirectory());
		fileChooser.setDialogTitle("Select encrypted file(s)");
		fileChooser.setMultiSelectionEnabled(true);
		fileChooser.setAcceptAllFileFilterUsed(false);

		FileNameExtensionFilter filter = new FileNameExtensionFilter("ENC files", "enc");
		fileChooser.addChoosableFileFilter(filter);

		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File[] files = fileChooser.getSelectedFiles();
			this.decryptFiles(files);
		}
	}

	/**
	 * Process each file from given array of File objects to be decrypted.
	 * @param files : Array of File objects in which each file's content will be decrypted.
	 */
	public void decryptFiles(File[] files) {
		FileReader reader;
		for (File file : files) {
			try {
				reader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(new FileInputStream(file), "UTF-8")
				);
				Scanner input = new Scanner(bufferedReader);
				while (input.hasNextLine()) {
					try {
						String[] lineArr = input.nextLine().split(" ");
						String message = lineArr[0];
						String key = lineArr[1];
						String seed = lineArr[2];
						this.decryptMessage(message, key, seed);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, "Error reading a line of "
								+ file.getName());
						this.gui.appendLog("Error reading a line of " + file.getName());
					}
				}
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null, "File not found or error for reading");
				this.gui.appendLog("File not found or error for reading.");
			} catch (UnsupportedEncodingException e) {
				JOptionPane.showMessageDialog(null, "Error recognizing encoding charset.");
				this.gui.appendLog("Error recognizing encoding charset.");
			}
		}
	}
}