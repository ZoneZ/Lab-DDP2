package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Farhan Azyumardhi Azmi - 1706979234
 * GUI constructed using NetBeans WindowBuilder.
 */
public class XoxoView {

	/**
	 * A field that used to be the input of the
	 * message that wants to be encrypted/decrypted.
	 */
	private JTextField messageField;

	/**
	 * A field that used to be the input of the key string.
	 * It is a Kiss Key if it is used as the encryption.
	 * It is a Hug Key if it is used as the decryption.
	 */
	private JTextField keyField;

	/**
	 * A field to be the input of the seed
	 */
	private JTextField seedField;

	/**
	 * A field that used to display any log information such
	 * as you click the button, an output file succesfully
	 * created, etc.
	 */
	private JTextArea logField;

	/**
	 * A button that when it is clicked, it encrypts the message.
	 */
	private JButton encryptButton;

	/**
	 * A button that when it is clicked, it decrpyts the message.
	 */
	private JButton decryptButton;

	private JLabel titleLabel;
	private JLabel messageFieldLabel;
	private JLabel keyFieldLabel;
	private JLabel seedFieldLabel;
	private JLabel logFieldLabel;
	private JScrollPane scrollPane;
	private JButton browseButton;

	/**
	 * Class constructor that initiates the GUI.
	 */
	public XoxoView() {
		this.initGui();
	}

	/**
	 * Constructs the GUI.
	 */
	private void initGui() {
		JFrame frame = new JFrame("XoXo Encryptor-Decryptor");
		titleLabel = new JLabel();
		messageFieldLabel = new JLabel();
		keyFieldLabel = new JLabel();
		messageField = new JTextField();
		keyField = new JTextField();
		seedFieldLabel = new JLabel();
		seedField = new JTextField();
		encryptButton = new JButton();
		decryptButton = new JButton();
		logFieldLabel = new JLabel();
		logField = new JTextArea();
		scrollPane = new JScrollPane();
		browseButton = new JButton();

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setBackground(new Color(204, 255, 204));

		titleLabel.setFont(new Font("Microsoft Tai Le", Font.BOLD, 18));
		titleLabel.setText("XoXo Encryptor-Decryptor");

		messageFieldLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		messageFieldLabel.setText("Insert Your Message");

		messageField.setFont(new Font("Tahoma", Font.PLAIN, 14));

		keyFieldLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		keyFieldLabel.setText("Insert Your Key");

		keyField.setFont(new Font("Tahoma", Font.PLAIN, 14));

		seedFieldLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		seedFieldLabel.setText("Insert Seed");

		seedField.setFont(new Font("Tahoma", Font.PLAIN, 14));

		GroupLayout groupLayout1 = new GroupLayout(panel1);
		panel1.setLayout(groupLayout1);
		groupLayout1.setHorizontalGroup(
				groupLayout1.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 0, Short.MAX_VALUE)
		);
		groupLayout1.setVerticalGroup(
				groupLayout1.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 0, Short.MAX_VALUE)
		);

		GroupLayout groupLayout2 = new GroupLayout(panel2);
		panel2.setLayout(groupLayout2);
		groupLayout2.setHorizontalGroup(
				groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 0, Short.MAX_VALUE)
		);
		groupLayout2.setVerticalGroup(
				groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGap(0, 0, Short.MAX_VALUE)
		);

		encryptButton.setBackground(new Color(102, 255, 255));
		encryptButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		encryptButton.setText("ENCRYPT!");
		encryptButton.setToolTipText("Encrypt the message");

		decryptButton.setBackground(new Color(255, 204, 204));
		decryptButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		decryptButton.setText("DECRYPT!");
		decryptButton.setToolTipText("Decrypt the message");

		logField.setColumns(20);
		logField.setRows(5);
		logField.setEditable(false);
		logField.setFont(new Font("Tahoma", Font.BOLD, 14));
		scrollPane.setViewportView(logField);

		logFieldLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		logFieldLabel.setText("Program Log");

		browseButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		browseButton.setText("Browse .enc Files...");
		browseButton.setToolTipText("Browse for file(s) with .enc extension to decrypt their contents");

		GroupLayout layout = new GroupLayout(frame.getContentPane());
		frame.getContentPane().setLayout(layout);

		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(panel1, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(panel2, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGroup(layout.createSequentialGroup()
												.addGap(22, 22, 22)
												.addGroup(layout.
														createParallelGroup(GroupLayout.Alignment.LEADING)
														.addGroup(layout.createSequentialGroup()
																.addComponent(messageFieldLabel)
																.addGap(45, 45, 45)
																.addComponent(messageField,
																		GroupLayout.PREFERRED_SIZE,
																		277,
																		GroupLayout.PREFERRED_SIZE))
														.addComponent(logFieldLabel)
														.addGroup(layout.createSequentialGroup()
																.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
																		.addComponent(encryptButton,
																				GroupLayout.PREFERRED_SIZE,
																				150, GroupLayout.PREFERRED_SIZE)
																		.addComponent(scrollPane,
																				GroupLayout.PREFERRED_SIZE,
																				217,
																				GroupLayout.PREFERRED_SIZE))
																.addGap(47, 47, 47)
																.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																		.addComponent(decryptButton,
																				GroupLayout.PREFERRED_SIZE,
																				150,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(browseButton)))
														.addGroup(layout.createSequentialGroup()
																.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
																		.addComponent(keyFieldLabel)
																		.addComponent(seedFieldLabel))
																.addGap(73, 73, 73)
																.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
																		.addComponent(keyField,
																				GroupLayout.DEFAULT_SIZE,
																				277,
																				Short.MAX_VALUE)
																		.addComponent(seedField)))))
										.addGroup(layout.createSequentialGroup()
												.addGap(155, 155, 155)
												.addComponent(titleLabel)))
								.addContainerGap(79, Short.MAX_VALUE))
		);

		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(panel1, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(panel2, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGap(13, 13, 13)
								.addComponent(titleLabel)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(messageFieldLabel)
										.addComponent(messageField, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGap(13, 13, 13)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addGap(3, 3, 3)
												.addComponent(keyFieldLabel))
										.addComponent(keyField, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
										.addComponent(seedFieldLabel,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(seedField,
												GroupLayout.PREFERRED_SIZE,
												23, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 56,
										Short.MAX_VALUE)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(encryptButton, GroupLayout.PREFERRED_SIZE,
												72, GroupLayout.PREFERRED_SIZE)
										.addComponent(decryptButton, GroupLayout.PREFERRED_SIZE,
												72, GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addComponent(logFieldLabel)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addGap(18, 18, 18)
												.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(layout.createSequentialGroup()
												.addGap(27, 27, 27)
												.addComponent(browseButton,
														GroupLayout.PREFERRED_SIZE,
														63, GroupLayout.PREFERRED_SIZE)))
								.addGap(46, 46, 46))
		);

		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Gets the message from the message field.
	 *
	 * @return The input message string.
	 */
	public String getMessageText() {
		return messageField.getText();
	}

	/**
	 * Gets the key text from the key field.
	 *
	 * @return The input key string.
	 */
	public String getKeyText() {
		return keyField.getText();
	}

	/**
	 * Gets the seed text from the key field.
	 *
	 * @return The input key string.
	 */
	public String getSeedText() {
		return seedField.getText();
	}

	/**
	 * Appends a log message to the log field.
	 *
	 * @param log The log message that wants to be
	 *            appended to the log field.
	 */
	public void appendLog(String log) {
		logField.append(log + '\n');
	}

	/**
	 * Sets an ActionListener object that contains
	 * the logic to encrypt the message.
	 *
	 * @param listener An ActionListener that has the logic
	 *                 to encrypt a message.
	 */
	public void setEncryptFunction(ActionListener listener) {
		encryptButton.addActionListener(listener);
	}

	/**
	 * Sets an ActionListener object that contains
	 * the logic to decrypt the message.
	 *
	 * @param listener An ActionListener that has the logic
	 *                 to decrypt a message.
	 */
	public void setDecryptFunction(ActionListener listener) {
		decryptButton.addActionListener(listener);
	}

	/**
	 * Sets an ActionListener object that contains
	 * the logic to browse for .enc files.
	 *
	 * @param listener An ActionListener that has the logic
	 *                 to browse for .enc files.
	 */
	public void setBrowseButtonFunction(ActionListener listener) {
		browseButton.addActionListener(listener);
	}
}