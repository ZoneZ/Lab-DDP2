package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Finds and gets an event by name from given arraylist of events
     * @param name : event's name to be searched
     * @return Event object if the searched event is found, null if otherwise
     */
    public Event getEvent(String name) {
        Event returnedEvent = null;
        for (Event event : events) {
            if (event.getName().equalsIgnoreCase(name)) {
                returnedEvent = event;
            }
        }
        return returnedEvent;
    }

    /**
     * Finds and gets a user by name
     * @param name : user's name to be searched
     * @return User object if the searched user is found, null if otherwise
     */
    public User getUser(String name) {
        User returnedUser = null;
        for (User user : this.users) {
            if (user.getName().equalsIgnoreCase(name)) {
                returnedUser = user;
            }
        }
        return returnedUser;
    }

    /**
     * Adds an event to the event system if it's not already in the system
     * @param name : name of the event
     * @param startTimeStr : start time of the event with "yyyy-mm-dd_hh:mm:ss" format
     * @param endTimeStr : end time of the event with "yyyy-mm-dd_hh:mm:ss" format
     * @param costPerHourStr : cost per hour of the event
     * @return log message according to each condition
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
       String out = "";
       if (this.getEvent(name) != null) {
           out += String.format("Event %s sudah ada!", name);
       } else {
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
           LocalDateTime startTime = LocalDateTime.parse(startTimeStr, formatter);
           LocalDateTime endTime = LocalDateTime.parse(endTimeStr, formatter);
           BigInteger costPerHour = new BigInteger(costPerHourStr);
           if (startTime.isAfter(endTime)) {
               out += "Waktu yang diinputkan tidak valid!";
           } else {
               this.events.add(new Event(name, startTime, endTime, costPerHour));
               out += String.format("Event %s berhasil ditambahkan!", name);
           }
       }
       return out;
    }

    /**
     * Adds a user to the event system if it's not already in the system
     * @param name : name of the user
     * @return log message according to each condition
     */
    public String addUser(String name)
    {
        String out = "";
        if (this.getUser(name) != null) {
            out += String.format("User %s sudah ada!", name);
        } else {
            this.users.add(new User(name));
            out += String.format("User %s berhasil ditambahkan!", name);
        }
        return out;
    }

    /**
     * Registers an event to a designated user if the event is not overlapping with
     * any registered event the user has
     * @param userName : name of the user
     * @param eventName : name of the event
     * @return log message according to each condition
     */
    public String registerToEvent(String userName, String eventName)
    {
        String out = "";
        User user = this.getUser(userName);
        Event event = this.getEvent(eventName);
        if (user == null && event == null) {
            out += String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!",
                    userName, eventName);
        } else {
            if (user == null) {
                out += String.format("Tidak ada pengguna dengan nama %s!", userName);
            } else {
                if (event == null) {
                    out += String.format("Tidak ada acara dengan nama %s!", eventName);
                } else {
                    if (!user.addEvent(event)) {
                        out += String.format("%s sibuk sehingga tidak dapat menghadiri %s!",
                                userName, eventName);
                    } else {
                        out += String.format("%s berencana menghadiri %s!",
                                userName, eventName);
                    }
                }
            }
        }
        return out;
    }
}