package lab9.user;

import lab9.event.Event;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        boolean overlapFound = false;
        if (this.events.size() > 0) {
            for (Event event : this.events) {
                if (event.overlapsWith(newEvent)) {
                    overlapFound = true;
                }
            }
        }

        if (overlapFound) {
            return false;
        } else {
            this.events.add(newEvent);
            return true;
        }
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> returnedList = new ArrayList<>();
        returnedList.addAll(this.events);
        returnedList.sort(new Comparator<Event>() {
            @Override
            public int compare(Event event1, Event event2) {
                return event1.getStartTime().compareTo(event2.getStartTime());
            }
        });
        return returnedList;
    }

    /**
     * Returns the total cost of every event this user plans to attend
     * @return
     */
    public BigInteger getTotalCost() {
        BigInteger totalCost = BigInteger.ZERO;
        for (Event event : this.events) {
            totalCost = totalCost.add(event.getCost());
        }
        return totalCost;
    }
}