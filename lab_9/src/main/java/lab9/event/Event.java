package lab9.event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;

    /** Start time of event */
    private LocalDateTime startTime;

    /** End time of event */
    private LocalDateTime endTime;

    /** Cost per hour of event */
    private BigInteger costPerHour;

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }


    /**
     * Accessor for startTime field.
     * @return start time of this event instance
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Accessor for endTime field.
     * @return end time of this event instance
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Accessor for costPerHour field.
     * @return cost per hour of this event instance
     */
    public BigInteger getCost() {
        return costPerHour;
    }

    /**
     * Constructor for Event instance
     * @param name : name of the event
     * @param startTime : start time of the event
     * @param endTime : end time of the event
     * @param costPerHour : the cost per hour of the event
     */
    public Event(String name, LocalDateTime startTime, LocalDateTime endTime, BigInteger costPerHour) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }

    /**
     * Return informations of this event instance
     * @return name, start time, end time, and cost per hour of this event
     */
    @Override
    public String toString() {
        DateTimeFormatter printFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return String.format("%s\nWaktu mulai: %s\nWaktu selesai: %s\nBiaya kehadiran: %s",
                this.name,
                this.startTime.format(printFormatter),
                this.endTime.format(printFormatter),
                String.valueOf(this.costPerHour));
    }

    /**
     * Checks if this event instance overlaps with other event instance
     * @param other : Event instance which is to be compared
     * @return true if overlaps, else if otherwise
     */
    public boolean overlapsWith(Event other) {
        if (this.startTime.isEqual(other.getStartTime())) {
            return true;
        } else if (this.startTime.isBefore(other.getStartTime())) {
            if (this.endTime.isBefore(other.getStartTime())
                || this.endTime.isEqual(other.getStartTime())) {
                    return false; 
            } else {
                return true;
            }
        } else {
            if (other.getEndTime().isBefore(this.startTime) 
                || other.getEndTime().isEqual(this.startTime)) {
                    return false;
            } else {
                return true;
            }
        }
    }
}