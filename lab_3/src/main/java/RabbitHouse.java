import java.util.Scanner;

public class RabbitHouse {
    
    public static int bonusSum = 0; // Class variable to count how many rabbits not infected by palindromitis

    // Static method used for "normal" command
    public static int breedingSum(int nameLength) {
        int sum = 1;
        if (nameLength <= 1) { // Base case
            return sum; 
        } else { // Recursive steps
            sum += (nameLength * breedingSum(nameLength-1)); 
        }
        return sum;
    }

    /* Static method to check for palindrome,
    inspired by a snippet from http://componentsprogramming.com/palindromes/ */
    public static boolean palindromeCheck(String name) {
        int length = name.length();
        for (int i = 0; i < length/2; ++i) {
            if (name.charAt(i) != name.charAt(length-i-1)) {
                return false;
            }
        }
        return true;
    }

    /* Static method to count how many master rabbit's children that do not get infected by palindromitis,
    and recursively checks their own children  */
    public static void notPalindromeCount(String name) {
        if (!palindromeCheck(name)) {
            for (int i = 0; i < name.length(); i++) {
                if (i == 0) {
                    String tempName = name.substring(1, name.length());
                    if (!palindromeCheck(tempName)) {
                        RabbitHouse.bonusSum += 1;
                        notPalindromeCount(tempName);
                    }
                }   
                else if (i == name.length()-1) {
                    String tempName = name.substring(0, name.length()-1);
                    if (!palindromeCheck(tempName)) {
                        RabbitHouse.bonusSum += 1;
                        notPalindromeCount(tempName);
                    }
                }
                else {
                    String tempName = name.substring(0, i) + name.substring(i+1, name.length());
                    if (!palindromeCheck(tempName)) {
                        RabbitHouse.bonusSum += 1;
                        notPalindromeCount(tempName);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.print("Masukkan Perintah: ");
            String command = input.nextLine();
            String[] commandArr = command.split(" ");
            String rabbitName = commandArr[1];
            if (rabbitName.length() <= 10) {
                if (commandArr[0].equals("normal")) {
                    System.out.println("Jumlah kelinci saat semua kelinci selesai berkembang biak: ");
                    System.out.print(breedingSum(rabbitName.length()));
                    break;
                }
                else if (commandArr[0].equals("palindrom")) {
                    if (palindromeCheck(rabbitName)) {
                        System.out.println("Kelinci terkena penyakit palindromitis.");
                        System.out.println("Jumlah total kelinci: " + 0);
                        break;
                    }
                    else {
                        RabbitHouse.bonusSum += 1;
                        notPalindromeCount(rabbitName);
                        System.out.println("Jumlah kelinci yang tidak terkena penyakit palindromitis: ");
                        System.out.print(RabbitHouse.bonusSum);
                        break;    
                    }
                }
                else {
                    System.out.println("Input salah. Silahkan masukkan perintah kembali");
                    continue;
                }    
            }
            else {
                System.out.println("Jumlah karakter pada nama kelinci lebih dari 10!");
                continue;
            }
        } 
            
    }
}