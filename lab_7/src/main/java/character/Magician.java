package character;

public class Magician extends Human {

	public Magician(String name, int hp) {
		super(name, hp);
	}

	/**
     * Method untuk membakar chara lain. Jika chara mati karena terbakar, 
	 * atau sudah mati kemudian dibakar, statusnya akan "matang".
     * @param enemy : object Player yang akan dibakar
     * @return result : String kembalian dari hasil serangan, sesuai deskripsi soal
     */
	public String burn(Player enemy) {
		String returnedLog = "";
		if (enemy instanceof Magician) {
			enemy.setHp(enemy.getHp() - 20);
		} else enemy.setHp(enemy.getHp() - 10);
		returnedLog = "Nyawa " + enemy.getName() + " " + enemy.getHp();

		if (enemy.getHp() <= 0) {
			enemy.setHp(0);
			enemy.setRoasted(true);
			enemy.setIsDead(true);
			returnedLog = "Nyawa " + enemy.getName() + " " + enemy.getHp() + " dan matang";
		}
		return returnedLog;
	}
}