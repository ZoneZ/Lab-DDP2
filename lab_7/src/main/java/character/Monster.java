package character;

public class Monster extends Player {

	private String roar;

	public void setRoar(String roar) {
		this.roar = roar;
	}

	public String roar() {
		return this.roar;
	}

	public Monster(String name, int hp, String roar) {
		super(name, 2 * hp);
		this.roar = roar;
	}

	public Monster(String name, int hp) {
		this(name, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
	}
}