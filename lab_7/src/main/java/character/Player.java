package character;

import java.util.ArrayList;

public class Player{

	protected String name;
	protected int hp;
	protected ArrayList<Player> diet = new ArrayList<>();
	protected boolean isRoasted;
	protected boolean isEaten;
	protected boolean isDead;

	public void setName(String name) {
		this.name = name;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public void setDiet(Player diet) {
		this.diet.add(diet);
	}

	public void setRoasted(boolean roasted) {
		this.isRoasted = roasted;
	}

	public void setEaten(boolean eaten) {
		this.isEaten = eaten;
	}
	
	public void setIsDead(boolean status) {
		this.isDead = status;
	}

	public String getName() {
		return this.name;
	}

	public int getHp() {
		return this.hp;
	}

	public ArrayList<Player> getDiet() {
		return this.diet;
	}

	public boolean isRoasted() {
		return this.isRoasted;
	}

	public boolean isEaten() {
		return this.isEaten;
	}

	public boolean isDead() {
		return this.isDead;
	}

	public Player(String name, int hp) {
		this.name = name;
		this.hp = (hp > 0) ? hp : 0;
		this.isDead = (hp <= 0) ? true : false;
		this.isRoasted = false;
		this.isEaten = false;
	}

     /**
     * Method untuk menyerang player lain.
     * @param enemy : object Player yang akan diserang
     * @return String : kembalian dari hasil serangan, sesuai deskripsi soal
     */
	public String attack(Player enemy) {
		if (enemy instanceof Magician) {
			enemy.setHp(enemy.getHp() - 20);
		} else enemy.setHp(enemy.getHp() - 10);

		if (enemy.getHp() <= 0) {
			enemy.setHp(0);
			enemy.setIsDead(true);
		}
		return "Nyawa " + enemy.getName() + " " + enemy.getHp();
	}

	/**
     * Method untuk mengecek apakah chara ini dapat memakan chara lain
     * @param food : object Player yang akan dimakan
     * @return boolean : kondisi apakah Player ini bisa memakan Player food
     */
	public boolean canEat(Player food) {
		if (this instanceof Human || this instanceof Magician) {
			if (food instanceof Monster && food.isDead() && food.isRoasted()) {
				return true;
			} else return false;
		} else {
			if (food.isDead()) return true;
			else return false;
		}
	}

	/**
     * Method untuk memakan chara lain. Melibatkan method canEat() terlebih dahulu
     * @param enemy : object Player yang akan diserang
     * @return String result kembalian dari hasil serangan, sesuai deskripsi soal
     */
	public String eat(Player food) {
		if (this.canEat(food)) {
			this.setHp(this.hp + 15);
			this.diet.add(food);
			food.setEaten(true);
			return this.getName() + " memakan " + food.getName() + "\n"
					+ "Nyawa " + this.getName() + " kini " + this.getHp();
		} else {
			return this.getName() + " tidak bisa memakan " + food.getName();
		}
	}

	/**
     * Method untuk meng-return daftar chara mana saja yang sudah dimakan.
     * @return String returnedDiet berupa chara (bisa lebih dari satu) yang sudah dimakan.
     */
	public String diet() {
		String returnedDiet = "";
		if (this.diet.size() == 0) returnedDiet = "Belum memakan siapa siapa";
		else {
			returnedDiet = "Memakan ";
			for (int i = 0; i < this.diet.size(); i++) {
				returnedDiet += this.diet.get(i)
											 .getClass()
											 .getSimpleName() + " "
								  + this.diet.get(i)
											 .getName();
				if (i != this.diet.size() - 1) returnedDiet += ", ";
			}
		}
		return returnedDiet;
	}

	/**
     * Method untuk meng-return status dari chara
     * @return status dari chara. Terdiri dari nama class, nama, hp,
	 * status hidup/mati, dan daftar diet dari chara.
     */
	public String status() {
		String printedStatus = (this.isDead()) ? "Sudah meninggal dengan damai" : "Masih hidup";
		return this.getClass().getSimpleName() + " " + this.name + "\nHP: " + this.hp + "\n"
				+ printedStatus + "\n" + this.diet();
	}
}