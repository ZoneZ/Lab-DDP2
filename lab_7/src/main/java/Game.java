import character.Player;
import character.Human;
import character.Magician;
import character.Monster;
import java.util.ArrayList;

public class Game {

    private ArrayList<Player> playerList = new ArrayList<>();
    private ArrayList<Player> toBeEatenList = new ArrayList<>(); // ArrayList kandidat chara yang dapat dimakan
    
    /**
     * Fungsi untuk mencari karakter
     * @param name : nama karakter yang ingin dicari
     * @return object Player yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        Player returnedplayer = null;
        for (Player player : this.playerList) {
            if (player.getName().equals(name)) {
                returnedplayer = player;
            }
        }
        return returnedplayer;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param chara : nama karakter yang ingin ditambahkan
     * @param tipe : tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param hp :  hp dari karakter yang ingin ditambahkan
     * @return String hasil keluaran dari penambahan karakter. Contoh : "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (this.find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            Player newPlayer;
            if (tipe.equals("Human")) {
                newPlayer = new Human(chara, hp);
            } else if (tipe.equals("Magician")) {
                newPlayer = new Magician(chara, hp);
            } else {
                newPlayer = new Monster(chara, hp);
            }
            this.playerList.add(newPlayer);
            this.toBeEatenList.add(newPlayer);
            return chara + " ditambah ke game";
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param chara : nama karakter yang ingin ditambahkan
     * @param tipe : tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param hp : hp dari karakter yang ingin ditambahkan
     * @param roar : teriakan dari karakter
     * @return String hasil keluaran dari penambahan karakter. Contoh : "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (this.find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            if (tipe.equals("Monster")) {
                Player newPlayer = new Monster(chara, hp, roar);
                this.playerList.add(newPlayer);
                this.toBeEatenList.add(newPlayer);
                return chara + " ditambah ke game";
            } else return "Tipe " + chara + " tidak sesuai";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param chara : character yang ingin dihapus
     * @return String hasil keluaran dari game
     */
    public String remove(String chara){
        if (this.find(chara) != null) {
            this.playerList.remove(this.find(chara));
            return chara + " dihapus dari game";
        } else return "Tidak ada " + chara;
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * @param chara : character yang ingin ditampilkan statusnya
     * @return String hasil keluaran dari game
     */
    public String status(String chara) {
        String returnedLog = "";
        if (this.find(chara) != null) {
           returnedLog = this.find(chara).status();
        } else return "Tidak ada " + chara;
        return returnedLog;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String returnedLog = "";
        if (this.playerList.size() == 0) returnedLog = "Tidak ada pemain";
        else {
            for (Player player : this.playerList) {
                returnedLog += player.status() + "\n";
            }
        }
        return returnedLog;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param chara : Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String returnedLog = "";
        if (this.find(chara) != null) {
            returnedLog = this.find(chara).diet();
        } else returnedLog =  "Tidak ada " + chara;
        return  returnedLog;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String returnedLog = "Daftar Karakter yang Sudah Dimakan: \n";
        ArrayList<Player> eatenList = new ArrayList<>();

        for (Player player : this.toBeEatenList) {
            if (player.isEaten()) eatenList.add(player);
        }

        for (Player eatenPlayer : eatenList) {
            returnedLog += (eatenPlayer.getClass().getSimpleName() + " " + eatenPlayer.getName() + "\n");
        }

        return returnedLog;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param meName : nama dari character yang sedang dimainkan
     * @param enemyName : nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        if (meName.equals(enemyName)) return meName + " tidak bisa menyerang dirinya sendiri";
        else {
            if (this.find(meName) != null) {
                if (this.find(enemyName)!= null) {
                    Player mePlayer = this.find(meName);
                    Player enemyPlayer = this.find(enemyName);
                    if (mePlayer.isDead()) return mePlayer.getName() + " tidak bisa menyerang "
                                                  + enemyPlayer.getName();
                    else {
                        return mePlayer.attack(enemyPlayer);
                    }
                } else return "Tidak ada " + enemyName;
            } else return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param meName : nama dari character yang sedang dimainkan
     * @param enemyName : nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        if (meName.equals(enemyName)) return meName + " tidak bisa membakar dirinya sendiri";
        else {
            if (this.find(meName) != null) {
                if (this.find(enemyName) != null) {
                    Player mePlayer = this.find(meName);
                    Player enemyPlayer = this.find(enemyName);
                    if (mePlayer.isDead()) return mePlayer.getName() + " tidak bisa membakar "
                                                  + enemyPlayer.getName();
                    else {
                        if (mePlayer instanceof Magician) {
                            return ((Magician) mePlayer).burn(enemyPlayer);
                        } else return meName + " bukan seorang magician";
                    }
                } else return "Tidak ada " + enemyName;
            } else return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param meName : nama dari character yang sedang dimainkan
     * @param enemyName : nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        String returnedLog = "";
        if (meName.equals(enemyName)) {
            returnedLog = meName + " tidak bisa memakan dirinya sendiri";
        } else {
            if (this.find(meName) != null) {
                if (this.find(enemyName) != null) {
                    Player mePlayer = this.find(meName);
                    Player enemyPlayer = this.find(enemyName);
                    if (mePlayer.isDead()) return mePlayer.getName() + " tidak bisa memakan "
                                                + enemyPlayer.getName();
                    else {
                        if (mePlayer instanceof Human) {
                            returnedLog = ((Human) mePlayer).eat(enemyPlayer);
                        } else returnedLog = mePlayer.eat(enemyPlayer);
                        
                        if (enemyPlayer.isEaten()) {
                            this.playerList.remove(enemyPlayer);
                        }
                    } 
                } else returnedLog = "Tidak ada " + enemyName;
            } else returnedLog =  "Tidak ada " + meName;
        }
        return returnedLog;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param meName : nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        String returnedRoar = "";
        if (this.find(meName) != null) {
            Player player = this.find(meName);
            if (player instanceof Monster) {
                returnedRoar = ((Monster) player).roar();
            } else returnedRoar = meName + " tidak bisa berteriak";
        } else returnedRoar = "Tidak ada " + meName;
        return returnedRoar;
    }


    /**
     * Method untuk mencetak menu dalam bentuk Tree
     * @return String hasil Tree dalam format, misalkan:
     * B memakan C,
     * B memakan D,
     * A memakan B,
     * A memakan E, 
     * Maka hasil keluarannya adalah A << [B << [C | D] | E]
     * Tanda "A << [B]" artinya A memakan B 
     */
    public String cetakMenu() {
        String out = "";
        for (int i = 0; i < this.playerList.size(); i++) {
            out += cetakTree(this.playerList.get(i));
            out += "\n";
            out += "=============\n";
        }
        return out;
    }

    /**
     * Method helper untuk mencetak satu "rantai makanan" secara rekursif
     * @param player : Player yang ingin dicetak rantai makanannya
     * @return String hasil satu rantai makanan
     */
    public String cetakTree(Player player) {
        String out = "";
        if (player.getDiet().size() == 0) {
            out += player.getName();
        } else {
            out += player.getName() + " <-- [";
            for (int i = 0; i < player.getDiet().size(); i++) {
                if (i == player.getDiet().size()-1) {
                    out += cetakTree(player.getDiet().get(i));
                } else {
                    out += cetakTree(player.getDiet().get(i)) + " | ";
                }
            }
            out += "]";
        }
        return out;
    }
}