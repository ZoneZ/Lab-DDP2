package customer;

import java.util.ArrayList;
import java.util.Arrays;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Customer {
    private String nama;
    private int umur;
    private String gender;
    private ArrayList<Ticket> boughtTicket = new ArrayList<>();
    private ArrayList<Ticket> watchedTicket = new ArrayList<>();

    public void setNama(String namaBaru) { this.nama = namaBaru; }
    public void setUmur(int umurBaru) { this.umur = umurBaru; }
    public void setGender(String genderBaru) { this.gender = genderBaru; }
    public void addBoughtTicket(Ticket ticket) { this.boughtTicket.add(ticket); }

    public String getNama() { return this.nama; }
    public int getUmur() { return this.umur; }
    public String getGender() { return this.gender; }
    public ArrayList<Ticket> getBoughtTicket() { return this.boughtTicket; }

    public Customer(String nama, String gender, int umur) {
        this.nama = nama;
        this.gender = gender;
        this.umur = umur;
    }
    
    /* Method untuk memesan tiket. 
     * Jika tiket yang dicari ada, maka akan meng-return object tiket tersebut
     */
    public Ticket orderTicket(Theater theater, String judul, String hari, String jenis) {
        boolean ticketFound = false;
        Ticket returnedTicket = null;
        for (Ticket ticket: theater.getTicket()) {
            if (ticket.getFilm().getJudul().equals(judul) &&
                ticket.getJadwalTayang().equals(hari) &&
                ticket.getJenisTiket().equals(jenis)) {
                    ticketFound = true;
                    if (this.getUmur() >= ticket.getFilm().getMinimalUsia()) {
                        System.out.format("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d",
                                      this.getNama(), judul, ticket.getJenisTiket(), 
                                      theater.getNama(), hari, ticket.getHargaTiket());
                        System.out.println();
                        theater.setSaldo(ticket.getHargaTiket());
                        this.boughtTicket.add(0, ticket);
                        theater.getTicket().remove(ticket);
                        returnedTicket = ticket;
                    } else {
                        System.out.format("%s masih belum cukup umur untuk menonton %s dengan rating %s\n",
                                          this.getNama(), judul, ticket.getFilm().getRating());
                    }
            }

            if (ticketFound == true) break;
        }

        if (ticketFound == false) {
            System.out.format("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n",
                              judul, jenis, hari, theater.getNama());
        }
        return returnedTicket;
    }

    // Method untuk mencari film di suatu bioskop
    public void findMovie(Theater theater, String judul) {
        boolean movieFound = false;
        for (Movie movie: theater.getMovie()) {
            if (movie.getJudul().equals(judul)) {
                movieFound = true;
                System.out.println(movie);
                break;
            }
        }

        if (movieFound == false) {
            System.out.format("Film %s yang dicari %s tidak ada di bioskop %s\n",
                              judul, this.getNama(), theater.getNama());
        }
    }

    // Method untuk meng-refund tiket bila tiket tersebut ditonton
    public void cancelTicket(Theater selectedTheater) {
        Ticket cancelledTicket = this.boughtTicket.get(0);
        boolean movieFound = false;

        for (int i = 0; i < selectedTheater.getMovie().length; i++) {
            if (selectedTheater.getMovie()[i].equals(cancelledTicket.getFilm())) {
                movieFound = true;
            }
        }

        if (!movieFound == true) {
            System.out.format("Maaf tiket tidak bisa dikembalikan, %s tidak tersedia dalam %s\n",
                              cancelledTicket.getFilm().getJudul(),
                              selectedTheater.getNama());
        } else {
            if (this.watchedTicket.contains(cancelledTicket)) {
                System.out.format("Tiket tidak bisa dikembalikan karena film %s sudah ditonton oleh %s\n",
                                  cancelledTicket.getFilm().getJudul(), this.getNama());
            } else {
                if (selectedTheater.getSaldo() < cancelledTicket.getHargaTiket()) {
                    System.out.format("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop %s lagi tekor...\n",
                                      selectedTheater.getNama());
                } else {
                    selectedTheater.setSaldo(-(cancelledTicket.getHargaTiket()));
                    System.out.format("Tiket film %s dengan waktu tayang %s jenis %s dikembalikan ke bioskop %s\n",
                                      cancelledTicket.getFilm().getJudul(), cancelledTicket.getJadwalTayang(),
                                      cancelledTicket.getJenisTiket(), selectedTheater.getNama());
                    this.boughtTicket.remove(0);
                    selectedTheater.addTicket(cancelledTicket);
                }
            }
        }
    }

    // Method untuk menonton film dari tiket yang sudah dimiliki
    public void watchMovie(Ticket ticket) {
        this.watchedTicket.add(ticket);
        System.out.format("%s telah menonton film %s\n",
                          this.getNama(), ticket.getFilm().getJudul());
    }
}