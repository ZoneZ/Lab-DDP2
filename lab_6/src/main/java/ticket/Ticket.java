package ticket;

import movie.Movie;
import theater.Theater;
import customer.Customer;

public class Ticket {
    private Movie judulFilm;
    private String jadwalTayang;
    private String jenisTiket;
    private int hargaTiket;
    private static final double TAMBAHAN_HARGA_3D = 0.2;

    public void setJadwalTayang(String jadwalBaru) { this.jadwalTayang = jadwalBaru; }
    public void setJenisTiket(String jenisBaru) { this.jenisTiket = jenisBaru; }
    public void setHargaTiket(int hargaBaru) { this.hargaTiket = hargaBaru; }

    public Movie getFilm() { return this.judulFilm; }
    public String getJadwalTayang() { return this.jadwalTayang; }
    public String getJenisTiket() { return this.jenisTiket; }
    public int getHargaTiket() { return this.hargaTiket; }
    public double getTambahan3D() { return Ticket.TAMBAHAN_HARGA_3D; }

    public Ticket(Movie judul, String jadwal, boolean is3D) {
        this.judulFilm = judul;
        this.jadwalTayang = jadwal;
        if (this.jadwalTayang.equals("Sabtu") | this.jadwalTayang.equals("Minggu")) {
            this.hargaTiket = 100000;
        } else {
            this.hargaTiket = 60000;
        }

        if (is3D == true) {
            double hargaTambahan = ((double) this.hargaTiket) * Ticket.TAMBAHAN_HARGA_3D;
            this.hargaTiket += (int) hargaTambahan;
            this.jenisTiket = "3 Dimensi";
        } else {
            this.jenisTiket = "Biasa";
        }
    }

    // Method untuk mencetak informasi tiket
    public String toString() {
        return "------------------------------------------------------------------\n" +
               "Film            : " + this.getFilm().getJudul() + "\n" +
               "Jadwal Tayang   : " + this.getJadwalTayang() + "\n" +
               "Jenis           : " + this.getJenisTiket() + "\n" + 
               "------------------------------------------------------------------\n";
    } 

}