package theater;

import java.util.ArrayList;
import java.util.Arrays;
import movie.Movie;
import ticket.Ticket;

public class Theater {
    private String nama;
    private Movie[] movieList;
    private ArrayList<Ticket> ticketList;
    private int saldo;

    public void setNama(String namaBaru) { this.nama = namaBaru; }
    public void setSaldo(int saldo) { this.saldo += saldo; }
    public void addTicket(Ticket ticket) { this.ticketList.add(ticket); }

    public String getNama() { return this.nama; }
    public Movie[] getMovie() { return this.movieList; }
    public ArrayList<Ticket> getTicket() { return this.ticketList; }
    public int getSaldo() { return this.saldo; }

    public Theater(String nama, int saldo, ArrayList<Ticket> tickets, Movie[] movies) {
        this.nama = nama;
        this.saldo = saldo;
        this.ticketList = tickets;
        this.movieList = movies;
    }

    // Method untuk mencetak informasi teater
    public void printInfo() {
        System.out.println("------------------------------------------------------------------"); 
        System.out.println("Bioskop                 : " + this.getNama());
        System.out.println("Saldo Kas               : " + String.valueOf(this.getSaldo()));
        System.out.println("Jumlah Tiket Tersedia   : " + String.valueOf(this.getTicket().size()));
        System.out.print("Daftar Film Tersedia    : ");
        
        for (int i = 0; i < this.getMovie().length; i++) {
            System.out.print(this.getMovie()[i].getJudul());
            if (i != (this.getMovie().length - 1)) {
                System.out.print(", ");
            } else System.out.print("\n");
        }
        System.out.println("------------------------------------------------------------------");
    }

    // Method untuk mencetak total pendapatan dari Koh Mas
    public static void printTotalRevenueEarned(Theater[] theaters) {
        int pendapatanTotal = 0;
        for (Theater theater: theaters) {
            pendapatanTotal += theater.getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + pendapatanTotal);
        System.out.println("------------------------------------------------------------------");

        for (Theater theater: theaters) {
            System.out.println("Bioskop     : " + theater.getNama());
            System.out.println("Saldo Kas   : Rp. " + theater.getSaldo());
            System.out.println();
        }
        System.out.println("------------------------------------------------------------------");
    }
}