package movie;

public class Movie {

    private String judul;
    private String genre;
    private int durasi;
    private String rating;
    private byte minimalUsia;
    private String jenis;

    public void setJudul(String judulBaru) { this.judul = judulBaru; }
    public void setGenre(String genreBaru) { this.genre = genreBaru; }
    public void setDurasi(int durasiBaru) { this.durasi = durasiBaru; }
    public void setRating(String ratingBaru) { this.rating = ratingBaru; }
    public void setMinimalUsia(byte usiaBaru) { this.minimalUsia = usiaBaru; }
    public void setJenis(String jenisBaru) { this.jenis = jenisBaru; }

    public String getJudul() { return this.judul; }
    public String getGenre() { return this.genre; }
    public int getDurasi() { return this.durasi; }
    public String getRating() { return this.rating; }
    public byte getMinimalUsia() { return this.minimalUsia; }
    public String getJenis() { return this.jenis; }

    public Movie(String judul, String rating, int durasi, 
                 String genre, String jenis) {
                    this.judul = judul;
                    this.rating = rating;
                    this.durasi = durasi;
                    this.genre = genre;
                    this.jenis = jenis;

                    if (this.rating.equals("Umum")) this.minimalUsia = 0;
                    else if (this.rating.equals("Remaja")) this.minimalUsia = 13;
                    else this.minimalUsia = 17;
                }
                
    // Method untuk mencetak informasi film
    public String toString() {
        return "------------------------------------------------------------------\n" + 
               "Judul   : " + this.getJudul() + "\n" +
               "Genre   : " + this.getGenre() + "\n" +
               "Durasi  : " + String.valueOf(this.getDurasi()) + " menit\n" +
               "Rating  : " + this.getRating() + "\n" +
               "Jenis   : " + "Film " + this.getJenis() + "\n" +
               "------------------------------------------------------------------\n";
    }

    // Method untuk override equals method (mengecek kesamaan antarfilm)
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } 
        if (this.getClass() != other.getClass()) {
            return false;
        }
        if (!this.judul.equals(((Movie) other).getJudul())) {
            return false;
        }
        if (!this.genre.equals(((Movie) other).getGenre())) {
            return false;
        }
        if (this.durasi != ((Movie) other).getDurasi()) {
            return false;
        }
        if (!this.rating.equals(((Movie) other).getRating())) {
            return false;
        }
        if (!this.jenis.equals(((Movie) other).getJenis())) {
            return false;
        }

        return true;
    }
 }